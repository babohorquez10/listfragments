package com.example.testfragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ExaminadosFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ExaminadosFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ExaminadosFragment : Fragment() {

    lateinit var contactsView: View
    lateinit var myContactList: RecyclerView

    lateinit var contactsref: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        contactsView = inflater.inflate(R.layout.fragment_examinados, container, false)

        myContactList = contactsView.findViewById<RecyclerView>(R.id.contacts_list)
        myContactList.layoutManager = LinearLayoutManager(context)


        contactsref = FirebaseDatabase.getInstance().reference.child("contactos")

        return contactsView
    }

    override fun onStart() {
        super.onStart()

        var options = FirebaseRecyclerOptions.Builder<Contacts>().setQuery(contactsref, Contacts::class.java).build()

       // val adapter = FirebaseRecyclerAdapter<Contacts, ContactsViewHolder()>{

        }


}
