package com.example.testfragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main.*

data class Movie(val title: String, val year: Int)

    class MainFragment : Fragment() {

        private val mNicolasCageMovies = listOf(
            Movie("Raising Arizona", 1987),
            Movie("Vampire's Kiss", 1988),
            Movie("Con Air", 1997),
            Movie("Gone in 60 Seconds", 1997),
            Movie("National Treasure", 2004),
            Movie("The Wicker Man", 2006),
            Movie("Ghost Rider", 2007),
            Movie("Knowing", 2009)
        )

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            retainInstance = true
        }

        override fun onCreateView(inflater: LayoutInflater,
                                  container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_main, container, false)

        // populate the views now that the layout has been inflated
        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            // RecyclerView node initialized here
            list_recycler_view.apply {
                // set a LinearLayoutManager to handle Android
                // RecyclerView behavior
                layoutManager = LinearLayoutManager(activity)
                // set the custom adapter to the RecyclerView
                adapter = ListAdapter(mNicolasCageMovies)
            }
        }

        companion object {
            fun newInstance(): MainFragment = MainFragment()
        }
    }
